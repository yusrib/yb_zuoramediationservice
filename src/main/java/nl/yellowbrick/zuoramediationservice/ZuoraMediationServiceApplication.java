package nl.yellowbrick.zuoramediationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZuoraMediationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuoraMediationServiceApplication.class, args);
	}

}
